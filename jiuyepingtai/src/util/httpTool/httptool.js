import axios from 'axios';
import { ElMessage } from 'element-plus';
import code from './httpCode';

// console.log(vueLs);
const Axios = axios.create({
	baseURL: process.env.VUE_APP_BASE_URL,
	timeout: 10000,
});

Axios.interceptors.request.use(
	config => {
		let token = localStorage.getItem('Authorization') || '';
		token = token ? JSON.parse(token).token : '';
		return {
			...config,
			headers: {
				...config.headers,
				Authorization: token,
			},
		};
	},
	error => {
		return Promise.reject(error);
	},
);

Axios.interceptors.response.use(
	response => {
		const { data } = response;
		if (data.code === 200) {
			return Promise.resolve(data);
		} else {
			ElMessage.error(data.msg);
			return Promise.reject(data);
		}
	},
	error => {
		switch (error.code) {
			case 'ECONNABORTED':
				{
					ElMessage.error('您的请求已超时请重新获取数据');
				}
				break;
		}
		code[error.code] && ElMessage.error(code[error.code]);
		return Promise.reject(error);
	},
);

export default Axios;
