import { createRouter, createWebHistory } from 'vue-router';
import Login from '@/views/login';
import { NoUserRoute } from './config';

const router = createRouter({
	history: createWebHistory(),
	routes: [
		{
			name: 'Login',
			path: '/login',
			meta: {
				title: '八维教育',
			},
			component: Login,
		},
		{
			name: 'HighPost',
			path: '/highPost',
			component: () => import('@/views/highPost'),
		},
	],
});

router.beforeEach((to, from, next) => {
	const myRoutes = JSON.parse(window.localStorage.getItem('bw_routes'));
	if (NoUserRoute.includes(to.name)) {
		next();
	}
	if (myRoutes.findIndex(({ name }) => name === to.name) > -1) {
		next();
	} else {
		next('/login');
	}
	document.title = to.meta.title;
});
// export const addRoutes = (config) => {
// 	if(Array.isArray(config)){
// 		config.map(item => {
// 			delete item.redirect;
// 			const components = require(`@/views/${item.component}`);
// 			router.addRoute({
// 				...item,
// 				component: components
// 			})
// 		})
// 		console.log(config);
// 	}
// }

export default router;
