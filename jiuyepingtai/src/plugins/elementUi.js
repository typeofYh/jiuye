import {
	ElMessage,
	ElForm,
	ElFormItem,
	ElInput,
	ElButton,
	ElTable,
	ElTableColumn,
	ElPopover,
} from 'element-plus';

export default {
	install(Vue) {
		Vue.use(ElMessage);
		Vue.use(ElForm);
		Vue.use(ElFormItem);
		Vue.use(ElInput);
		Vue.use(ElButton);
		Vue.use(ElTable);
		Vue.use(ElTableColumn);
		Vue.use(ElPopover);
	},
};
