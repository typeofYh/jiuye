export const getCaptchaImage = {
	url: '/dev-api/captchaImage',
	method: 'get',
};

export const login = {
	url: '/dev-api/login',
	method: 'post',
};

export const getRouters = {
	url: '/dev-api/getRouters',
	method: 'get',
};
