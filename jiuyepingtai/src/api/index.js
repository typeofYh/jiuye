import axios from '@/util/httpTool/httptool';

const dirdata = require.context('./config', true, /\.js$/);

/**
 * [reduce 生成api配置对象]
 *	dirdata.keys() [] => config 下所有的文件路径
 *  dirdata 本身就是一个方法(路径) 得到模块（js文件）对象
 *  {文件名:{文件内部定义的方法}}
 */
const res = dirdata.keys().reduce((val, item) => {
	const apis = dirdata(item);
	const nameSpeace = item.match(/\/(\w+)\.js$/)[1];
	val[nameSpeace] = Object.keys(apis).reduce((val, key) => {
		val[key] = (data = {}) => axios({ ...apis[key], data });
		return val;
	}, {});
	return val;
}, {});

export default res;
