module.exports = {
	lintOnSave: process.env.NODE_ENV === 'development',
	devServer: {
		proxy: {
			'/dev-api': {
				target: process.env.VUE_APP_BASE_URL,
				ws: true,
				changeOrigin: true,
			},
		},
	},
};
